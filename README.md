# Getting familiar with LaTeX

## Sources of information
Welp, there are many of them, but by far the most userufl are:
* Your search engine of choice
* [overleaf docs](https://overleaf.com/learn)
* [tex.stackexchange.com](https://tex.stackexchange.com)

## Editing and compilation
Basically, there are 2 ways to get things done, online and locally.

### Online
The most widely used online editor also happens to be by overleaf,
but if you don't want to register another useless account, 
[latexbase](https://latexbase.com/) is also quite cool.

### Locally
As for the "offline" editors, I'm only familiar with how things are done
in the terminal. Anyways, you'll need `texlive` (should be in the repositories
of the most distros) and a text/code editor. TexLive provides the `pdflatex`
tool, which in most cases can simply be run against your `.tex` file, and you'll
get an output pdf in case no critical errors were found.

Although, running it manually may be tedious, so I eventually installed 
[neotex](https://github.com/donRaphaco/neotex), which compiles the stuff
for me, and [zathura](https://wiki.archlinux.org/title/Zathura),
which is a pdf editor able to reload files when they're changed.

## Basic structure
### Documentclass
The sources typically begin with smth like
```
\documentclass[a4paper]{report}
```
Where `documentclass{}` tells LaTeX how to structure the document (basic fontsize,
paper format, margins, whatever), and in `[]` are the options (e.g, the default paper
format for some LaTeX distrubutions is set to US Letter, and we don't exactly want
this to happen). 

### Usepackage
Then there are a bunch of `\usepackage{}` commands. Those are pretty much the same
as libraries in programming, i.e. they let you do cool stuff without spending 
each free minute reinventing the wheel. For example, if one wants to type some stuff
in Russian, they would use the following packages:
```
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[russian]{babel}
```

### Title
When all the necessary packages are used, depending on the type of the document
there may be something like 
```
\title{Some title}
\author{Somebody}
\date{\today}
```

### Document
It's pretty clear what those commands stand for, but adding just them
won't display anything, instead one should put `\maketitle` command in the
```
\begin{document}
 ...
\end{document}
```
environment. This environment is also where most of the writing happens. Also,
whatever is written beneath the `\end{document}` is simply ignored.

### Headers
Inside the document environment one can (and in most cases should)
further structuralize the information by dividing it into `\part{}`-s, `\chapter{}`-s, 
`\section{}`-s, `\subsection{}`-s, `\subsubsection{}`-s, etc. Those commands
simply denote headers of various sizes, and are somewhat specific to the documentclass
used (e.g. some of them might lack chapters or parts, and others might have completely
different commands). In `{}` user writes the name of the corresponding header. By default
most of the headers (except subsubsection, I guess) are numbered. If the desired behaviour
is different, one should write `*` after the commands (e.g. `\subsection*{Some title}`).

### Table of contents, page breaks.
Just like in word or google docs, the headers can be used to automatically make up
a table of contents, the corresponding `\tableofcontents` command is used in such cases.
It may be followed by `\newpage` if the writer wants to see it on a separate page (btw,
both work independently as well).

### Newlines
A single new line in the `.tex` file does nothing, the actual `\n` is inserted either with `\\`
or by leaving an empty line in the file.

### Math
The mathematical formulas are written either between 2 dollar signs (tex style)
or inside of `\(\)` (LaTeX style) if they are single-line, or
```
$$$
$$$
```
and
```
\[
\]
```
for multi-line ones.
